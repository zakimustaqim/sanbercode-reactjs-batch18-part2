import React from 'react';

class Nama extends React.Component {
  render() {
    return <td> {this.props.nama}</td>;
  }
}

class Harga extends React.Component {
  render() {
    return <td> {this.props.harga}</td>;
  }
}

class Berat extends React.Component {
  render() {
    return <td> {this.props.berat}</td>;
  }
}


let dataHargaBuah = [
  {nama: "Semangka", harga: 10000, berat: 1000},
  {nama: "Anggur", harga: 40000, berat: 500},
  {nama: "Strawberry", harga: 30000, berat: 400},
  {nama: "Jeruk", harga: 30000, berat: 1000},
  {nama: "Mangga", harga: 30000, berat: 500}
]

class Tugas10 extends React.Component {
  render() {
    return (
     <div>
        <h1> Tabel Harga Buah </h1>
        <table style={{"text-align":"left", width:"40%", margin:"0 auto 20px", border: "2px solid", borderColor:"rgb(170,170,170)"}}>
          <tr style={{"backgroundColor": "rgb(170,170,170)", "textAlign": "center"}}>
            <th>
              Nama
            </th>
            <th>
              Harga
            </th>
            <th>
              Berat
            </th>
          </tr>
          {dataHargaBuah.map(item => {
              return (
                <tr style={{"backgroundColor":"rgb(255,127,80)"}}>
                  <Nama nama={item.nama} />
                  <Harga harga={item.harga} />
                  <Berat berat={item.berat} />
                </tr>
              )
            }            
          )}
        </table>
      </div>
    )
  }
}

export default Tugas10