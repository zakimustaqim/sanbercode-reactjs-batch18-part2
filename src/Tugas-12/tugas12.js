import React, {Component} from "react"

class ListsForm extends Component{

  constructor(props){
    super(props)
    this.state ={
     arrayObject : [
        {nama: "Semangka", harga: 10000, berat: 1000},
        {nama: "Anggur", harga: 40000, berat: 500},
        {nama: "Strawberry", harga: 30000, berat: 400},
        {nama: "Jeruk", harga: 30000, berat: 1000},
        {nama: "Mangga", harga: 30000, berat: 500}
      ],
     inputNama: "",
     inputHarga: "",
     inputBerat: "",
     index: -1,
    }


    this.handleEdit = this.handleEdit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeNama = this.handleChangeNama.bind(this);
    this.handleChangeHarga = this.handleChangeHarga.bind(this);
    this.handleChangeBerat = this.handleChangeBerat.bind(this);
  }

  handleChangeNama(event){
    this.setState({
      inputNama: event.target.value
    });
  }


  handleChangeHarga(event){
    this.setState({
      inputHarga: event.target.value
    });
  }

  handleChangeBerat(event){
    this.setState({
      inputBerat: event.target.value
    });
  }

  handleEdit(event){
    let index = event.target.value;
    let itemArray = this.state.arrayObject[index];
    this.setState({
      inputNama: itemArray.nama,
      inputHarga: itemArray.harga,
      inputBerat: itemArray.berat,
      index: index
    })
  }

  handleDelete(event){
    let index = event.target.value;
    this.state.arrayObject.splice(index, 1)
    this.setState({arrayObject: this.state.arrayObject})
  }


  handleSubmit(event){
    event.preventDefault()
    console.log(this.state.inputNama)
    let index = this.state.index;
    let inputNama = this.state.inputNama;
    let inputHarga = this.state.inputHarga;
    let inputBerat = this.state.inputBerat;
    let arrayObject = this.state.arrayObject;
    let lengthArrayObject = this.state.arrayObject.length;
    let objectData = {nama: inputNama, harga: inputHarga, berat: inputBerat}

    if (index === -1){
      this.setState({
        arrayObject: [...arrayObject, objectData],
      })
    }else{
      arrayObject[index] = objectData
      this.setState({
        arrayObject,
        inputNama: "",
        inputHarga: "",
        inputBerat: "",
        index: -1,
      })
    }
  }

  render(){
    return(
      <>
        <h1>Tabel Harga Buah</h1>
        <table style={{"text-align":"left", width:"40%", margin:"0 auto 20px", border: "2px solid", borderColor:"rgb(170,170,170)"}}>
          <thead>
            <tr style={{"backgroundColor": "rgb(170,170,170)", "textAlign": "center"}}>
              <th>Nama</th>
              <th>Harga</th>
              <th>Berat</th>
            </tr>
          </thead>
          <tbody>
              {
                this.state.arrayObject.map((val, index)=>{
                  return(                    
                    <tr style={{"backgroundColor":"rgb(255,127,80)"}}>
                      <td>{val.nama}</td>
                      <td>{val.harga}</td>
                      <td>{val.berat}</td>
                      <td width="110px">
                        <button  value={index} onClick={this.handleEdit} >Edit</button>
                        <button style={{marginLeft: "1em", marginRight: "0px"}} value={index} onClick={this.handleDelete}>Delete</button>
                      </td>
                    </tr>
                  )
                })
              }
          </tbody>
        </table>
        {/* Form */}
        <h1>Form Harga Buah </h1>
          <form onSubmit={this.handleSubmit}>
            <label style={{margin:"10px"}}>
              Masukkan nama buah:
            </label>          
            <input type="text" value={this.state.inputNama} required onChange={this.handleChangeNama}/>
            <label style={{margin:"10px"}}>
              Harga:
            </label>          
            <input type="text" value={this.state.inputHarga} required onChange={this.handleChangeHarga}/>
             <label style={{margin:"10px"}}>
              Berat:
            </label>          
            <input type="text" value={this.state.inputBerat} required onChange={this.handleChangeBerat}/>
            <button style={{margin:"10px"}}>submit</button>
          </form>
      </>
    )
  }
}

export default ListsForm