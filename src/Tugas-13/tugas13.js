import React, {useState, useEffect  } from "react"
import axios from "axios"


const HooksAxios = () => {

  const [arrayObject, setArrayObject] = useState(null);

  const [inputNama, setInputNama] = useState("");
  const [inputHarga, setInputHarga] = useState("");
  const [inputBerat, setInputBerat] = useState("");
  const [inputID, setInputID] = useState(null);

  useEffect( () => {
    if (arrayObject === null){
      console.log("masuk")
      axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
      .then(res => {
        let daftarBuah = res.data
        setArrayObject(
          daftarBuah.map(el=> {
            return {id: el.id, name: el.name, harga: el.price, berat: el.weight}
          })
        )     
      })
    }
  },[arrayObject])


  const handleChangeNama = (event) => {
    setInputNama(event.target.value)
  }

  const handleChangeHarga = (event) => {
    setInputHarga(event.target.value)
  }

  const handleChangeBerat = (event) => {
    setInputBerat(event.target.value)
  }

  const handleEdit = (event) => {
    var idBuah= parseInt(event.target.value)
    var daftarBuah = arrayObject.find(x=> x.id === idBuah)
    setInputNama(daftarBuah.name)
    setInputHarga(daftarBuah.harga)
    setInputBerat(daftarBuah.berat)
    setInputID(idBuah)
  }

  const handleDelete = (event) => {
    let idBuah = event.target.value;
    axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idBuah}`)
    .then(res => {
      var daftarBuah = arrayObject.filter(x=> x.id !== idBuah)
      setArrayObject(daftarBuah)
    })
  }


  const handleSubmit = (event) => {
    event.preventDefault();

    if(inputID === null){
       axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {name: inputNama, price: inputHarga, weight: inputBerat})
      .then(res => {
        var data = res.data
        setArrayObject([...arrayObject, {id: inputID, name: inputNama, harga:inputHarga, berat: inputBerat}])
        setInputNama("")
        setInputHarga("")
        setInputBerat("")
        setInputID(null)
      })

    }else{

      axios.put(`http://backendexample.sanbercloud.com/api/fruits/${inputID}`, {name: inputNama, harga: inputHarga, berat: inputBerat})
      .then(res => {
        var daftarBuah = arrayObject.map(x => {
          if (x.id === inputID){
            x.name = inputNama
            x.price = inputHarga
            x.weight = inputBerat
          }
          return x
        })
        setArrayObject(daftarBuah)
        setInputNama("")
        setInputHarga("")
        setInputBerat("")
        setInputID(null)
      })
    }
  }


  return(
    <>
      <h1>Tabel Harga Buah</h1>
      <table style={{"text-align":"left", width:"40%", margin:"0 auto 20px", border: "2px solid", borderColor:"rgb(170,170,170)"}}>
        <thead>
          <tr style={{"backgroundColor": "rgb(170,170,170)", "textAlign": "center"}}>
            <th>Nama</th>
            <th>Harga</th>
            <th>Berat</th>
          </tr>
        </thead>
        <tbody>
            { arrayObject !== null && (
              arrayObject.map((val, index)=>{
                return(                    
                  <tr style={{"backgroundColor":"rgb(255,127,80)"}}>
                    <td>{val.name}</td>
                    <td>{val.harga}</td>
                    <td>{val.berat}</td>
                    <td width="110px">
                      <button  value={val.id} onClick={handleEdit} >Edit</button>
                      <button style={{marginLeft: "1em", marginRight: "0px"}} value={val.id} onClick={handleDelete} >Delete</button>
                    </td>
                  </tr>
                )
              })
              )
            }
        </tbody>
      </table>
      {/* Form */}
      <h1>Form Harga Buah </h1>
        <form onSubmit={handleSubmit}>
          <label style={{margin:"10px"}}>
            Masukkan nama buah:
          </label>          
          <input type="text" value={inputNama} required onChange={handleChangeNama}/>
          <label style={{margin:"10px"}}>
            Harga:
          </label>          
          <input type="text" value={inputHarga} required onChange={handleChangeHarga}/>
           <label style={{margin:"10px"}}>
            Berat:
          </label>          
          <input type="text" value={inputBerat} required onChange={handleChangeBerat}/>
          <button style={{margin:"10px"}}>submit</button>
        </form>
    </>
  )
}

export default HooksAxios