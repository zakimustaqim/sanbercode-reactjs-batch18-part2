import React, {Component} from "react"

class Timer extends Component{
  constructor(props){
    super(props)
    var today =  new Date();
    this.state = {
      time: today.getHours() + ":" + today.getMinutes() + ":" +  today.getSeconds(),
      timer: 100,
      showTime: true
    }
  }

  componentDidMount(){
    if (this.props.start !== undefined){
      this.setState({time: this.props.start})
    }
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount(){
    clearInterval(this.timerID);
  }

  componentDidUpdate() {
    // stop if time equal with 10
    // if (this.state.time === 10){
    //   this.componentWillUnmount()      
    // }
  }

  tick() {
    var today =  new Date();
    this.setState({
      timer: this.state.timer - 1,
      time: today.getHours() + ":" + today.getMinutes() + ":" +  today.getSeconds()
    });
    
    if (this.state.timer < 0){
      this.setState({
        showTime: false
      });
      this.componentWillUnmount()
    }
  }

  render(){
    return (
      <>
        {
          this.state.showTime && (
            <div style={{display: "flex", "justify-content": "center"}}>
              <h1 style={{textAlign: "center", marginRight:"30px"}}>
                sekarang jam: {this.state.time}
              </h1>
              <h1 style={{textAlign: "center"}}>
                hitung mundur: {this.state.timer}
              </h1>
            </div>
          )
        }
      </>
    )

  }
}

export default Timer
