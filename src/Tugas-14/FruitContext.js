import React, { useState, createContext, useEffect } from "react";
import axios from "axios"

export const FruitContext = createContext();

export const FruitProvider = props => {
  const [fruits, setFruits] = useState(null);

  useEffect( () => {
    if (fruits === null){
      console.log("masuk")
      axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
      .then(res => {
        let daftarBuah = res.data
        setFruits(
          daftarBuah.map(el=> {
            return {id: el.id, name: el.name, harga: el.price, berat: el.weight}
          })
        )     
      })
    }
  },[fruits])

  return (
    <FruitContext.Provider value={[fruits, setFruits]}>
      {props.children}
    </FruitContext.Provider>
  );
};