import React, {useContext, useState} from "react"
import {FruitContext} from "./FruitContext"
import axios from "axios"

const FruitList = () =>{
  const [fruits, setFruits] = useContext(FruitContext)
  const [inputNama, setInputNama] = useState("");
  const [inputHarga, setInputHarga] = useState("");
  const [inputBerat, setInputBerat] = useState("");
  const [inputID, setInputID] = useState(null);

  const handleEdit = (event) => {
    var idBuah= parseInt(event.target.value)
    var daftarBuah = fruits.find(x=> x.id === idBuah)
    setInputNama(daftarBuah.name)
    setInputHarga(daftarBuah.harga)
    setInputBerat(daftarBuah.berat)
    setInputID(idBuah)
    console.log(inputNama)
  }

  const handleDelete = (event) => {
    let idBuah = event.target.value;
    axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idBuah}`)
    .then(res => {
      var daftarBuah = fruits.filter(x=> x.id !== idBuah)
      setFruits(daftarBuah)
    })
  }

  return(
    <>
      <h1>Tabel Harga Buah</h1>
      <table style={{"text-align":"left", width:"40%", margin:"0 auto 20px", border: "2px solid", borderColor:"rgb(170,170,170)"}}>
        <thead>
          <tr style={{"backgroundColor": "rgb(170,170,170)", "textAlign": "center"}}>
            <th>Nama</th>
            <th>Harga</th>
            <th>Berat</th>
          </tr>
        </thead>
        <tbody>
          { fruits !== null && (
            fruits.map((val, index)=>{
              return(                    
                <tr style={{"backgroundColor":"rgb(255,127,80)"}}>
                  <td>{val.name}</td>
                  <td>{val.harga}</td>
                  <td>{val.berat}</td>
                  <td width="110px">
                    <button  value={val.id} onClick={handleEdit} >Edit</button>
                    <button style={{marginLeft: "1em", marginRight: "0px"}} value={val.id} onClick={handleDelete} >Delete</button>
                  </td>
                </tr>
              )
            })
            )
          }
        </tbody>
      </table>
    </>
  )

}

export default FruitList