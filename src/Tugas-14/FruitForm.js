import React, {useContext, useState} from "react"
import {FruitContext} from "./FruitContext"
import axios from "axios"

const FruitForm = () =>{
  const [fruits, setFruits] = useContext(FruitContext)
  const [inputNama, setInputNama] = useState("");
  const [inputHarga, setInputHarga] = useState("");
  const [inputBerat, setInputBerat] = useState("");
  const [inputID, setInputID] = useState(null);

  const handleSubmit = (event) => {
    event.preventDefault();

    if(inputID === null){
       axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {name: inputNama, price: inputHarga, weight: inputBerat})
      .then(res => {
        var data = res.data
        setFruits([...fruits, {id: inputID, name: inputNama, harga:inputHarga, berat: inputBerat}])
        setInputNama("")
        setInputHarga("")
        setInputBerat("")
        setInputID(null)
      })

    }else{

      axios.put(`http://backendexample.sanbercloud.com/api/fruits/${inputID}`, {name: inputNama, harga: inputHarga, berat: inputBerat})
      .then(res => {
        var daftarBuah = fruits.map(x => {
          if (x.id === inputID){
            x.name = inputNama
            x.price = inputHarga
            x.weight = inputBerat
          }
          return x
        })
        setFruits(daftarBuah)
        setInputNama("")
        setInputHarga("")
        setInputBerat("")
        setInputID(null)
      })
    }
  }

  const handleChangeNama = (event) => {
    setInputNama(event.target.value)
  }

  const handleChangeHarga = (event) => {
    setInputHarga(event.target.value)
  }

  const handleChangeBerat = (event) => {
    setInputBerat(event.target.value)
  }

  return(
    <>

      <h1>Form Harga Buah </h1>
      <form onSubmit={handleSubmit}>
        <label style={{margin:"10px"}}>
          Masukkan nama buah:
        </label>          
        <input type="text" value={inputNama} required onChange={handleChangeNama}/>
        <label style={{margin:"10px"}}>
          Harga:
        </label>          
        <input type="text" value={inputHarga} required onChange={handleChangeHarga}/>
         <label style={{margin:"10px"}}>
          Berat:
        </label>          
        <input type="text" value={inputBerat} required onChange={handleChangeBerat}/>
        <button style={{margin:"10px"}}>submit</button>
      </form>
    </>
  )

}

export default FruitForm