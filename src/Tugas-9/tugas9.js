import React from 'react';

class Tugas9 extends React.Component {
  render() {
    return (
     <div style={{border: "1px solid", width: "40%", margin: "0 auto", borderRadius: "10px"}}>
        <h1> Form Pembelian Buah </h1>
        <form>
          <table style={{"text-align":"left", width:"75%", margin:"0 auto 20px"}}>
            <tr>
              <th>
                Nama Pelanggan
              </th>
              <td>
                <input type="text" id="name" name="name" />
              </td>
            </tr>
            <tr>
              <th style={{"vertical-align":"bottom","text-align":"left"}}>
                Daftar Item
              </th>
              <td>
                <input type="checkbox" id="semangka" name="buah" value="semangka" />
                <label for="semangka">Semangka</label><br/>
                <input type="checkbox" id="jeruk" name="buah" value="jeruk" />
                <label for="jeruk">Jeruk</label><br/>
                <input type="checkbox" id="nanas" name="buah" value="nanas" />
                <label for="nanas">Nanas</label><br/>
                <input type="checkbox" id="salak" name="buah" value="salak" />
                <label for="salak">Salak</label><br/>
                <input type="checkbox" id="anggur" name="buah" value="anggur" />
                <label for="anggur">Anggur</label>
              </td>
            </tr>
            <tr>
              <th>
                <input style={{marginTop:"10px", borderRadius:"10px", backgroundColor:"white"}} type="submit" value="Kirim" />
              </th>
            </tr>
          </table>
          
        </form>
      </div>
    )
  }
}

export default Tugas9