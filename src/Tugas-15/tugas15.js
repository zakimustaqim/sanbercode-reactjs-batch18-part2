import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Tugas9 from "../Tugas-9/tugas9"
import Tugas10 from "../Tugas-10/tugas10"
import Timer from "../Tugas-11/tugas11"
import ListsForm from "../Tugas-12/tugas12"
import HooksAxios from "../Tugas-13/tugas13"
import Fruit from "../Tugas-14/tugas14"


const Routes = () => {
  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li><Link to="/materi-9">Materi 9</Link></li>
            <li><Link to="/materi-10">Materi 10</Link></li>
            <li><Link to="/materi-11">Materi 11</Link></li>
            <li><Link to="/materi-12">Materi 12</Link></li>
            <li><Link to="/materi-13">Materi 13</Link></li>
            <li><Link to="/materi-14">Materi 14</Link></li>
            
          </ul>
        </nav>

        <Switch>
          {/* contoh route dengan component nya sebagai child */}
          <Route exact path="/">
            <Tugas9 />
          </Route>
          {/* contoh route dengan component nya sebagai props */}
          <Route exact path="/materi-9" component={Tugas9} />
          <Route exact path="/materi-10" component={Tugas10} />
          <Route exact path="/materi-11" component={Timer} />
          <Route exact path="/materi-12" component={ListsForm} />
          <Route exact path="/materi-13" component={HooksAxios} />
          <Route exact path="/materi-14" component={Fruit} />
            
        </Switch>
      </div>
    </Router>
  );
}

export default Routes
